package Fractales;


import javax.swing.JFrame;
import java.io.*;
import java.lang.*;
import java.awt.*;

public class Fractal extends JFrame{

    Fractal(){

        super("Cuadritos Recursivos");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setBackground(Color.black);
        setSize(600, 600);
        setVisible(true);
}
    public void drawFractal(int cx, int cy,int y, Graphics g){
        g.setColor(Color.red);
        g.drawRect(cx,cy,y,y); 
        if(y>10)

        {
            drawFractal(cx+3,cy+3,(y/2)-6,g); 
            drawFractal((cx+y/2)+3,cy+3,(y/2)-6,g); 
            drawFractal(cx+3,cy+(y/2)+3,(y/2)-6,g);
            drawFractal((cx+y/2)+3,cy+(y/2)+3,(y/2)-6,g); 
        }

    }

    public void paint(Graphics g){
        drawFractal(25,35,550,g);
    }

    public static void main(String[] args){
        new Fractal();
    }
}

